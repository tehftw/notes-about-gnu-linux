
	vim

how to move the cursor to the middle of the screen? I know I can use
`zz` to move the screen, so that the line is in the middle. But, I want
to move the cursor itself to the line that's middle on the screen.

I'm making a rudimentary plugin for an easier browsing -- instead of
having to move the cursor to the bottom/top of the screen, the whole
screen moves thanks to `set scrolloff=9999`. But, at the start i have to
manually move the cursor into the middle of the screen.

I use it so that I can comfortably use jk and arrows for easy
scrolling(`<c-e>` `<c-y>` quickly get unwieldy when reading anything
that has to be scrolled), and all the other movement commands.

---

	tmux , terminal

Q: I'm using xfce, and want to create a shortcut `Super + t` for starting up terminal with tmux. What command should I put it to execute upon shortcut? I tried `st tmux attach` , however it fails when there isn't any tmux session running(instead, the terminal appears for a fraction of a second and doesn't even let me see what the problem is).

Normal `st tmux` causes, which is .

For some time, I used the following in my .bashrc: 
```{bash}
# get into tmux at start
[ -z "$TMUX" ] && { tmux attach || exec tmux new-session; }
```

But I removed it, as it was inconvienent when I wanted to run the terminal without tmux. And to add to that, I couldn't use bash at all when my partition was full. Couldn't even login!


A: How I worked around it:

I created a script `start-tmux.sh`, which contains the following:

```
#!/bin/bash
# if not inside tmux, then start it.

if [ -z "$TMUX" ]; then
	tmux attach || exec tmux new-session
else
	echo "already in tmux!"
fi
```

And I execute `st start-tmux.sh`

---


