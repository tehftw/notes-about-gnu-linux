
---

	restart x server

```bash
sudo systemctl restart display-manager
```

---

	start x server

pass X11 server options, by preceding them with a double dash

```bash
startx -- vt7
```

---

https://unix.stackexchange.com/questions/408461/where-is-the-default-repeat-rate-for-xset-stored

`xset r rate 180 50`

---

	xbacklight

fix xbacklight not working with intel graphics

https://itsfoss.com/fix-brightness-ubuntu-1310/

`ls /sys/class/backlight/`

create/edit file `/usr/share/X11/xorg.conf.d/20-intel.conf`, and add lines:

```
Section "Device"
        Identifier  "card0"
        Driver      "intel"
        Option      "Backlight"  "intel_backlight"
        BusID       "PCI:0:2:0"
EndSection
```

---

	mouse emulation

http://en.linuxreviews.org/HOWTO_use_the_numeric_keyboard_keys_as_mouse_in_XOrg

add this line to /etc/X11/xorg.conf.d/00-keyboard.conf:
```
Option "XkbOptions" "keypad:pointerkeys"
```


or execute shell command:
```bash
setxkbmap -option keypad:pointerkeys
```

---



```bash
for t in /dev/tty[0-9]*; do setleds -D +num <$t; done
```

---



