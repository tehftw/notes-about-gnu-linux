
---

	xorg init start
if you're going into X11 through the TTY, you should use `exec startx` not `startx`. If you just use startx, then someone can just switch back to the TTY and hit ctrl+c to get access to your shell. With `exec startx`, your bash session was replaced with xorg, so it just knocks you back to the login screen if you hit ctrl+c.﻿ 

---

	losedows microsoft windows dualboot delete files
https://askubuntu.com/questions/988519/how-do-i-delete-windows-files-from-ubuntu
Q: How do I delete Windows files from Ubuntu?

A: Open a terminal Type `sudo mount -o remount,rw /disk/location /mount/location`

You just unmounted and remounted your disk as R/W so anyone can read - write. Make sure to do `sudo umount /disk/location` when you are done, because if anyone wants to do harm they can write to the disk now!

Try `lsblk` for your disk location and when you want to regenerate your fstab, just reboot.

---

Q: How to use `cp` command to exclude a specific directory?

A: `rsync` is fast and easy:

```bash
rsync -av --progress sourcefolder /destinationfolder --exclude thefoldertoexclude
```

You can use `--exclude` multiples times.

Also you can add `-n` for dry run to see what will be copied before performing real operation, and if everything is ok, remove `-n` from command line.

---

list all explicitily installed packages:

```bash
aptitude search '~i !~M' -F '%p' --disable-columns | sort -u > currentlyinstalled.txt
```

---

	uuid

to get uuid of partition:

```bash
blkid /dev/sda1
```

---

	XDG_CONFIG_HOME
you don't need to define it, unless you want to change the default( default: $HOME/.config )

https://superuser.com/questions/365847/where-should-the-xdg-config-home-variable-be-defined

---

 Tip for multiple patches: Open all the .diff in a text editor and check which one is the most complex/longest. Apply that one using patch or git. For the others, just open the code and the diff next to each other and do it manually. In the diff is says what file the change is, and around the lines you need to add an remove (prefix + and -) they show the code that should be around the code that needs change. That is enough information to apply the whole patch manually. 

The real power of suckless for me is that i can build on top of it myself, so i can really make it my own and have things no for which no plugin/patch is available for any terminal emulator.﻿ 
