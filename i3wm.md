	i3wm

---

	cannot enlarge urxvt width

problem: using resize mode, I could not enlarge the width of urxvt. turns out to be some strange quirk: I had i3wm configured to change the size in increments of 4 px. after changing it to 16 px, I could enlarge the width.

---
