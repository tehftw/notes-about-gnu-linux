
---

Q: how to delete empty lines?

A: command
```vim
:g/^$/d
```

`:g` will execute a command on lines which match a regex. The regex is 'blank line' and the command is `:d` (delete)

---

Q: how to append a sequence of number at the end of each line?

A: 

```vim
:%s/$/\=line('.')/
```

this will append the sequence of numbers at the end of each line. to add numbers in the begining do

```vim
:%s/^/\=line('.')/
```
