	Wrap Existing Text at 80 Characters in Vim
	by Dan Croak, 2013-04-07, updated on 2016-05-10
	https://thoughtbot.com/blog/wrap-existing-text-at-80-characters-in-vim

You have an existing block of text or code in vim. You want to re-format it to wrap to 80-characters.
```vim
:set textwidth=80
```

You might want this setting to apply automatically within certain file types like Markdown:

```vim
au BufRead,BufNewFile *.md setlocal textwidth=80
```

We have that setting in thoughtbot/dotfiles.

Select the lines of text you want to re-format with `v`, reformat with `gq`.

Learn more: `:help gq`
