https://superuser.com/questions/30216/why-create-many-partitions

---

about swap: https://help.ubuntu.com/community/SwapFaq

---


Q: why create many partitions?

A: There are several reasons:

* System robustness. If you have /home on a separate partition from /, then a regualr user can't fill up the / filesystyem, making it unusable for the rest of the system.
* Backups. It makes it easier to back up entire partitions, and to back up on different schedules. For instance, you might only need to do weekly backups of the system partition, but nightly backups of the /home filesystem
* System installs. You can have the same /home filesystem mounted by several different system images. And you can delete and rebuild / with out doing a backup/restore of /home or /local
* Disk optimization. Not as much of an issue with todays fast drives, but it used to be common practice to put the system filesystem on the inside tracks of the disk to speed up access
* Using multiple drives. Before the common availability of large drives, it was common to have little room on the system drive. So a separate drive was used for /home.
* NFS. When sharing data across multiple systems, it is common to do it on a filesystem basis.

---
