

---

	always have a tmux session on

1. (general) start terminal with `tmux attach -t base || tmux new -s base`


2. (bash , bashrc) For some time, I used the following in my .bashrc:
```bash
# get into tmux at start
[ -z "$TMUX" ] && { tmux attach || exec tmux new-session; }
```

note: inconvienent when wanting to run the terminal without tmux. and to add to that, I couldn't use bash at all when my partition was full. couldn't even login!

3. (bash, script)

I created a script `start-tmux.sh`, which contains the following:

```bash
#!/bin/bash
# if not inside tmux, then start it.

if [ -z "$TMUX" ]; then
	tmux attach || exec tmux new-session
else
	echo "already in tmux!"
fi
```

execute `st start-tmux.sh` (where `st` is your chosen terminal emulator. in my case, it's "suckless terminal")

---

	sync tmux panes	posted on 2012-08-22

If you have a tmux window divided into panes, you can use the synchronize-panes window option to send each pane the same keyboard input simultaneously

You can do this by switching to the appropriate window, typing your Tmux prefix (commonly Ctrl-B or Ctrl-A) and then a colon to bring up a Tmux command line, and typing:

```tmux
:setw synchronize-panes
```

You can optionally add on or off to specify which state you want; otherwise the option is simply toggled. This option is specific to one window, so it won’t change the way your other sessions or windows operate. When you’re done, toggle it off again by repeating the command.

This is an easy way to run interactive commands on multiple machines, perhaps to compare their speed or output, or if they have a similar setup a quick and dirty way to perform the same administrative tasks in parallel. It’s generally better practice to use Capistrano or Puppet for the latter.

---

https://www.hamvocke.com/blog/a-guide-to-customizing-your-tmux-conf/

Making tmux Pretty and Usable - A Guide to Customizing your tmux.conf
command line
2015-08-17

	less awkward prefix keys

Probably the most common change among tmux users is to change the prefix from the rather awkward C-b to something that’s a little more accessible. Personally I’m using C-a instead but note that this might interfere with bash’s “go to beginning of line” command1. On top of the C-a binding I’ve also remapped my Caps Lock key to act as Ctrl since I’m not using Caps Lock anyways. This allows me to nicely trigger my prefix key combo.

To change your prefix from C-b to C-a, simply add following lines to your tmux.conf:

```tmux
# remap prefix from 'C-b' to 'C-a'
unbind C-b
set-option -g prefix C-a
bind-key C-a send-prefix
```

	sane split commands

Another thing I personally find quite difficult to remember is the pane splitting commands. I mean, seriously? " to split vertically and % to split horizontally? Who’s supposed to memorize that? I find it helpful to have the characters as a visual representation of the split, so I chose | and - for splitting panes:

```tmux
# split panes using | and -
bind | split-window -h
bind - split-window -v
unbind '"'
unbind %
```

	easy config reloads

Since I’m experimenting quite often with my tmux.conf I want to reload the config easily. This is why I have a command to reload my config on r:

```tmux
# reload config file (change file location to your the tmux.conf you want to use)
bind r source-file ~/.tmux.conf
```

	Fast Pane-Switching

Switching between panes is one of the most frequent tasks when using tmux. Therefore it should be as easy as possible. I’m not quite fond of triggering the prefix key all the time. I want to be able to simply say M-<direction> to go where I want to go (remember: M is for Meta, which is usually your Alt key). With this modification I can simply press Alt-left to go to the left pane (and other directions respectively):

```tmux
# switch panes using Alt-arrow without prefix
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D
```

	Mouse mode


Enabling mouse mode allows you to select windows and different panes by simply clicking on them.

```tmux
# Enable mouse control (clickable windows, panes, resizable panes)
set -g mouse-select-window on
set -g mouse-select-pane on
set -g mouse-resize-pane on
```

Update for tmux 2.1:
As Jon Lillie pointed out in the comments, mouse mode has been rewritten in tmux 2.1. Once you are on tmux 2.1 (or later) you can activate the new mouse mode with a single command:

```tmux
# Enable mouse mode (tmux 2.1 and above)
set -g mouse on
```

The new mode is a combination of all the old mouse options and fixes the text selection issues as well.
Stop renaming windows automatically

I like to give my tmux windows custom names using the , key. This helps me naming my windows according to the context they’re focusing on. By default tmux will update the window title automatically depending on the last executed command within that window. In order to prevent tmux from overriding my wisely chosen window names I want to suppress this behavior:


```tmux
# don't rename windows automatically
set-option -g allow-rename off
```

Changing the look of tmux

Changing the colors and design of tmux is a little more complex than what I’ve presented so far. You’ll want tmux to give a consistent look which is why you most likely have to change the looks of quite a lot of elements tmux displays. This is why changes to the design often result in plenty of lines in your config. I can only recommend to put these into their own identifiable section within your tmux.conf to be able to change this block of config without accidentaly ripping out some of your precious custom key bindings. I’m using comments, starting with a # character to make it visible where the design changes start.

Credit where credit is due: I did not create the following design. /u/dothebarbwa was so kind to publish it on /r/unixporn so it’s his effort and all thanks have to go out to him. Thanks!

Depending on your color scheme (I’m using base16-ocean-dark) your resulting tmux will look something like this:

	themed tmux

```tmux
######################
### DESIGN CHANGES ###
######################

# loud or quiet?
set-option -g visual-activity off
set-option -g visual-bell off
set-option -g visual-silence off
set-window-option -g monitor-activity off
set-option -g bell-action none

#  modes
setw -g clock-mode-colour colour5
setw -g mode-attr bold
setw -g mode-fg colour1
setw -g mode-bg colour18

# panes
set -g pane-border-bg colour0
set -g pane-border-fg colour19
set -g pane-active-border-bg colour0
set -g pane-active-border-fg colour9

# statusbar
set -g status-position bottom
set -g status-justify left
set -g status-bg colour18
set -g status-fg colour137
set -g status-attr dim
set -g status-left ''
set -g status-right '#[fg=colour233,bg=colour19,bold] %d/%m #[fg=colour233,bg=colour8,bold] %H:%M:%S '
set -g status-right-length 50
set -g status-left-length 20

setw -g window-status-current-fg colour1
setw -g window-status-current-bg colour19
setw -g window-status-current-attr bold
setw -g window-status-current-format ' #I#[fg=colour249]:#[fg=colour255]#W#[fg=colour249]#F '

setw -g window-status-fg colour9
setw -g window-status-bg colour18
setw -g window-status-attr none
setw -g window-status-format ' #I#[fg=colour237]:#[fg=colour250]#W#[fg=colour244]#F '

setw -g window-status-bell-attr bold
setw -g window-status-bell-fg colour255
setw -g window-status-bell-bg colour1
```



---

	swap windows:




A1: The swap-window command is closest to what you want.

"Prefix :" (that is "Ctrl-B :" by default) brings you to the tmux-command prompt. There you enter:

swap-window -s 3 -t 1

to let window number 3 and window number 1 swap their positions.

To swap the current window with the top window, do:

swap-window -t 0

In the unlikely case of having no window at index 0, do:

move-window -t 0

(if base-index is 0, as it is by default).

You can bind that command to a key (T for "top" for example) by adding the following to your ~/.tmux.conf:

bind-key T swap-window -t 0


A2: Adding to Gareth's answer, you can use the following key bindings

bind-key -n C-S-Left swap-window -t -1
bind-key -n C-S-Right swap-window -t +1

Pressing Ctrl+Shift+Left (will move the current window to the left. Similarly right. No need to use the modifier (C-b).


---

	refresh tmux

add to .tmux.conf: `bind-key r source-file ~/.tmux.conf`

---

	zoom pane

key combo `prefix + z` to focus on a pane

---

	tips
https://www.sitepoint.com/10-killer-tmux-tips/

---







