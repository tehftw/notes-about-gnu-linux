done on 2018-11-18


---

problem: couldn't login into Xfce nor i3wm after creating a separate `/home` partition

cause: something with permissions. the whome `/home` was owned by root

solution: `chown -hvR [user] /home/[user]` (`-v` is for verbose, so not needed. not sure about `-v`)

---

