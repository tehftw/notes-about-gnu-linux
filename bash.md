bash tips, Q&A, etc.

1. add things that are concerning an interactive shell to .bashrc

---

	notes tips

* Variables and command substitutions must always be quoted or your script will choke on whitespace and strings like this \[*?.

---

https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash

Q: how to check if a variable is set in Bash?

A: (usually) the right way

```
if [ -z ${var+x} ]; then echo "var is unset"; else echo "var is set to '$var'"; fi
```

whe re ${var+x} is a parameter expansion which evaluates to nothing if var is unset, and substitutes the string x otherwise.

---


